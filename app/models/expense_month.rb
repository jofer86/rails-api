class ExpenseMonth < ApplicationRecord
  belongs_to :year
  has_many :expenses
  has_many :incomes

  def total_incomes
    income = incomes.sum(&:amount) / 100
    income.to_f
  end

  def total_expenses
    expense = expenses.sum(&:amount) / 100
    expense.to_f
  end

  def deduct_from_designated_amount(amount_to_deduct)
    self.designated_amount -= amount_to_deduct
    save!
  end

  def update_expense_amount(current_amount, new_amount)
    self.designated_amount += current_amount
    self.designated_amount -= new_amount
    save!
  end

  def add_to_designated_amount(amount_to_add)
    self.designated_amount = 0 if designated_amount.nil?
    self.designated_amount += amount_to_add
    save!
  end

  def update_income_amount(current_amount, new_amount)
    self.designated_amount -= current_amount
    self.designated_amount += new_amount
  end
end
