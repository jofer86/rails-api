class Year < ApplicationRecord
  has_many :expense_months


  def safe_save
    generate_expense_year
    save
  end

  private
  def generate_expense_year
    months = %w[january february march april may june july august september october november december]
    expense_months.create(
      months.map do |month|
        {
          name: month,
          designated_amount: 0,
          year_id: id
        }
      end
    )
  end
end
