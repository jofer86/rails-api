class Expense < ApplicationRecord
  belongs_to :expense_month

  def safe_save
    expense_month.deduct_from_designated_amount(amount)
    save
  end

  def safe_update(new_amount)
    expense_month.update_expense_amount(amount, new_amount)
    amount = new_amount
    save
  end
end
