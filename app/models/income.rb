class Income < ApplicationRecord
  belongs_to :expense_month

  def safe_save
    expense_month.add_to_designated_amount(amount)
    save
  end

  def safe_update(new_amount)
    expense_month.update_income_amount(amount, new_amount)
    amount = new_amount
    save
  end
end
