class Api::V1::YearsController < ApplicationController

  def index
    @years = Year.all.includes(:expense_months)

    render json: YearSerializer.new(@years).serializable_hash
  end

  def show
    @year = Year.includes(:expense_months).find(params[:id])
    render json: YearSerializer.new(@year).serializable_hash[:data][:attributes]
  end

  def create
    year = current_user.years.create(year_params)
    if year.safe_save
      render json: YearSerializer.new(year)
    else
      'hay mi chavo no jalo fijese'
    end
  end


  private
  def year_params
    params.require(:year).permit(:year, :id)
  end

  def set_year
    @year = Year.find(params[:id])
  end
end
