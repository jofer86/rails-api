class Api::V1::IncomesController < ApplicationController

  def create
    set_month
    income = @month.incomes.create(incomes_params)
    if income.safe_save
      render json: income
    end
  end

  def update

  end


  private

  def set_month
    month_id = params[:income][:expense_month_id]
    @month = ExpenseMonth.find(month_id)
  end

  def incomes_params
    params.require(:income).permit(:name, :amount, :id, :expense_month_id)
  end
end
