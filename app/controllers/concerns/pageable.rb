module Pageable
  extend ActiveSupport::Concern

  def paginate(collection)
    paginator.call(
      collection,
      params: pagination_params,
      base_url: request.url
    )
  end

  def paginator
    JSOM::Pagination::Paginator.new
  end

  def pagination_params
    params.permit![:page]
  end

  def render_collection(paginated_collection)
    options = { meta: paginated_collection.meta.to_h, links: paginated_collection.links.to_h }
    serialized_collection = serializer.new(paginated_collection.items, options)
    render json: serialized_collection, status: :ok
  end
end
