class YearSerializer
  include JSONAPI::Serializer
  attributes :year, :expense_months
  attribute :expense_months do |year, params|
    year.expense_months.map do |month|
      ExpenseMonthSerializer.new(month).serializable_hash[:data][:attributes]
    end
  end
end
