class ExpenseMonthSerializer
  include JSONAPI::Serializer
  attributes :name, :designated_amount

  attribute :total_expenses do |month, params|
    month.total_expenses
  end

  attribute :total_incomes do |month, params|
    month.total_incomes
  end

  attribute :designated_amount do |month, params|
    amount = month.designated_amount / 100
    amount.to_f
  end


end
