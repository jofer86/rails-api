import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './route/login/login.component';
import { MaterialModule } from "../material-module";
import {FormsModule} from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { YearComponent } from './route/year/year.component';
import { MonthComponent } from './route/year/month/month.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    YearComponent,
    MonthComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
