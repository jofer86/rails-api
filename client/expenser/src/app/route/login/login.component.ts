import { Component, OnInit } from '@angular/core';
import {Form} from "@angular/forms";
import {AuthorizationService, User} from "../../service/authorization.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User = {
    user: {
      email: '',
      password: ''
    }
  }
  constructor(private authorizationService: AuthorizationService) { }

  hide = true;


  ngOnInit(): void {
  }

  onSubmit(form: any) {
    let { email, password } = form.value;
    this.authorizationService.login(email, password).subscribe(data => {
      console.log(data);
    })

  }

}
