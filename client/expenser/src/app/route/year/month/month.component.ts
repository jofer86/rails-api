import { Component, OnInit } from '@angular/core';

export interface Month {
  name: string;
  designated_amount: number;
}

@Component({
  selector: 'app-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.scss']
})
export class MonthComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
