import {Component, Input, OnInit} from '@angular/core';
import { Month } from "./month/month.component";
import {YearService} from "../../service/year/year.service";
import {Observable} from "rxjs";


export interface Year {
  year: number;
  expense_months: Month[];
}

@Component({
  selector: 'app-year',
  templateUrl: './year.component.html',
  styleUrls: ['./year.component.scss']
})
export class YearComponent implements OnInit {
  year: Year;
  year$: Observable<Year>;

  constructor(private yearService: YearService) { }

  ngOnInit(): void {
    this.year$ = this.yearService.getYear(1);
  }

}
