import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import * as moment from 'moment';

export interface User {
  user: {
    email: string;
    password: string;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private http: HttpClient) { }

  login(email:string, password:string ) {
    return this.http.post<User>('http://localhost:3000/users/sign_in', {user: { email, password }});

      // this is just the HTTP call,
      // we still need to handle the reception of the token

  }
}
