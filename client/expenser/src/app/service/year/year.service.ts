import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import {Observable} from "rxjs";
import {Year} from "../../route/year/year.component";

@Injectable({
  providedIn: 'root'
})
export class YearService {

  constructor(private http: HttpClient) { }

  getYear(id: number): Observable<Year> {
    return this.http.get<Year>(`http://localhost:3000/api/v1/years/${id}`)
  }
}
