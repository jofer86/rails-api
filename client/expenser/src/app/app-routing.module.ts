import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./route/login/login.component";
import { YearComponent } from "./route/year/year.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'year/:id', component: YearComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
