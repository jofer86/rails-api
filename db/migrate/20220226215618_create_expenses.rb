class CreateExpenses < ActiveRecord::Migration[7.0]
  def change
    create_table :expenses do |t|
      t.string :name
      t.bigint :amount
      t.belongs_to :expense_month, index: { unique: false }, foreign_key: true

      t.timestamps
    end
  end
end
