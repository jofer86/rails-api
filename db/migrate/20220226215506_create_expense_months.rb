class CreateExpenseMonths < ActiveRecord::Migration[7.0]
  def change
    create_table :expense_months do |t|
      t.string :name
      t.bigint :designated_amount

      t.timestamps
    end
  end
end
