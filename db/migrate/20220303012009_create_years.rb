class CreateYears < ActiveRecord::Migration[7.0]
  def change
    create_table :years do |t|
      t.integer :year

      t.timestamps
    end

    change_table :expense_months do |t|
      t.belongs_to :year, index: { unique: false }, foreign_key: true
    end

    change_table :years do |t|
      t.belongs_to :user, index: { unique: false }, foreign_key: true
    end
  end
end
