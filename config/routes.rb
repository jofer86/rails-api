Rails.application.routes.draw do
  devise_for :users,
  controllers: { 
                sessions: 'users/sessions',
                registrations: 'users/registrations'
               }, defaults: { format: :json }
  get '/members-data', to: 'members#show'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :years, only: %i[show index create]
      resources :incomes, only: %i[create update]
    end
  end
end
